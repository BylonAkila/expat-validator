TARGET =	validator.o
CC =		gcc
CFLAGS =	-Wall -Wextra -std=c1x -pedantic -D_GNU_SOURCE

# To link, you must use, in order to link with expat:
# LDFLAGS =	-lexpat

OPT_LEVEL?=-O3

all: $(TARGET)

%.o: %.c
	$(CC) $(CFLAGS) $(OPT_LEVEL) $< -c -o $@

clean:
	rm -f $(TARGET)

call: clean all
