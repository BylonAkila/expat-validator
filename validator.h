#ifndef EXPAT_VALIDATOR_HEADER_INCLUDED
#define EXPAT_VALIDATOR_HEADER_INCLUDED 1

#include <expat.h>

                      /* We redefine it as int to avoid pedantic */
                      /* error on argument promotion             */
                      /* Values are compatible with XML_Bool as  */
                      /* we can easily see in the define below!  */
typedef unsigned int VC_Bool;
#define VC_TRUE   ((VC_Bool) XML_TRUE)
#define VC_FALSE  ((VC_Bool) XML_FALSE)


typedef struct VC_st          VC_Stack;
typedef struct VC_cp          VC_Content;
typedef struct VC_hd          VC_Handlers;
typedef struct _vc_Internal * VC_Validator;

enum VC_Error
{
  VC_ERROR_NONE=50,
  VC_ERROR_ELEMENT_NOT_ALLOWED_HERE,
  VC_ERROR_ELEMENT_ENDED_PREMATURELY,
  VC_ERROR_TEXT_NOT_ALLOWED_HERE,
  VC_ERROR_PI_NOT_ALLOWED_IN_EMPTY_ELEMENT,
  VC_ERROR_COMMENT_NOT_ALLOWED_IN_EMPTY_ELEMENT,
  VC_ERROR_OUT_OF_MEMORY,
  VC_ERROR_ABORTED,
  VC_ERROR_ALREADY_FINISHED,
  VC_ERROR_INTERNAL_ERROR
};

enum VC_Validating {
  VC_INITIALIZED,
  VC_VALIDATING,
  VC_FINISHED,
  VC_SUSPENDED
};

typedef struct {
  enum VC_Validating validating   ;
  VC_Bool            finalCallback;
} VC_ValidationStatus;


struct VC_st {
  VC_Stack        *pParent ;
  VC_Content      *pThis   ;
  VC_Content      *pStep   ;
};

typedef enum VC_Error
(XMLCALL * VC_ValidationErrorHandler)(enum VC_Error error, void *pUserData, const char *el);

struct VC_hd
{
  VC_ValidationErrorHandler        error  ;
  XML_StartElementHandler          start  ;
  XML_EndElementHandler            end    ;
  XML_CharacterDataHandler         text   ;
  XML_ProcessingInstructionHandler pi     ;
  XML_CommentHandler               comment;
};

struct VC_cp
{
  union
    {
      XML_Char   *            name    ;  /* Name for an element   */
      VC_Content *            element ;  /* element for a vertex  */
    }                       p;
  VC_Handlers              *pCallbacks;
  unsigned int              flags     ;
  unsigned int              nEdges    ;
  VC_Content              **ppVertex  ;
};


#define VC_F_FINAL         1
#define VC_F_MIXED         2

                                         /* Macro to help graph  */
                                         /* initialisation.      */
                                         /* See examples in test */
                                         /* program.             */
#define VC_EDGE(p)  ( sizeof(p) / sizeof(p[0]) ), &p[0]


VC_Validator XMLCALL
VC_ValidatorCreate( XML_Parser p,
                    VC_Content *pTopElement
                  );

void XMLCALL
VC_ValidatorStart( VC_Validator pvci );

void XMLCALL
VC_ValidatorFree(VC_Validator pvci);



void XMLCALL
VC_SetUserData( VC_Validator pvci ,
               void *userData);

void * XMLCALL
VC_GetUserData(VC_Validator pvci);

void XMLCALL
VC_UseValidatorAsHandlerArg( VC_Validator pvci );



enum VC_Error XMLCALL
VC_GetErrorCode( VC_Validator pvci );

const XML_LChar * XMLCALL
VC_ErrorString(enum VC_Error errCode);

XML_LChar * XMLCALL
VC_ExpatValidatorVersion( void );


enum XML_Status XMLCALL
VC_StopValidator( VC_Validator pvci, VC_Bool resumable );


void XMLCALL
VC_GetValidationStatus( VC_Validator pvci, VC_ValidationStatus *pStatus);


VC_Stack * XMLCALL
VC_GetCurrentStack( VC_Validator pvci );


#endif /* EXPAT_VALIDATOR_HEADER_INCLUDED */
