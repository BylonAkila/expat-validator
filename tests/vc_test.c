/*=====================================================================
 Copyright (C) 2011 Alain BENEDETTI aka Zakhar @ ubuntu.fr

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published byr
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>
=====================================================================*/
/*====================================================================================\
| Date: 2012-08-26                                                                    |
|======                                                                               |
|-------------------------------------------------------------------------------------|
| Author: Alain BENEDETTI aka Zakhar                                                  |
|========                                                                             |
|-------------------------------------------------------------------------------------|
| Version: 1.0.0                                                                      |
|=========                                                                            |
|-------------------------------------------------------------------------------------|
| Function:                                                                           |
|==========                                                                           |
| This is the test suite for expat-validator                                          |
|                                                                                     |
| It uses the following DTD, 'top' being the topmost element:                         |
|                                                                                     |
|  <!ELEMENT top (test)+ >                                                            |
|  <!ELEMENT test ( a, ( (b,c?) | (d*,e) )*, f? )+ >                                  |
|  <!ELEMENT a (#PCDATA, a, b, c, d) >                                                |
|  <!ELEMENT b (#PCDATA) >                                                            |
|  <!ELEMENT c (#PCDATA) >                                                            |
|  <!ELEMENT d (#PCDATA) >                                                            |
|  <!ELEMENT e EMPTY >                                                                |
|  <!ELEMENT f EMPTY >                                                                |
|                                                                                     |
| Let's consider the XML stream begins like this:                                     |
|                                                                                     |
| <top>                                                                               |
|   <test>                                                                            |
|     <a>Text in a</a>                                                                |
|     <b>Text in b</b>                                                                |
|                                                                                     |
| Now, the next element can be ANY of the elements from a to f or even none (meaning  |
| an ending of </test>) and in all cases the XML will be valid. Note that this is NOT |
| "non-deterministic" as in all the cases we will follow only one and only one        |
| determined path in the DTD.                                                         |
|                                                                                     |
| Then this xml showing all possible cases from this start MUST be valid              |
|                                                                                     |
| <?xml version="1.0" encoding="utf-8"?>                                              |
| <top>                                                                               |
|   <test>                                                                            |
|     <a>Text in a</a>                                                                |
|     <b>Text in b</b>                                                                |
|   </test>                                                                           |
|   <test>                                                                            |
|     <a>Text in a</a>                                                                |
|     <b>Text in b</b>                                                                |
|     <a>Text in a again</a>                                                          |
|   </test>                                                                           |
|   <test>                                                                            |
|     <a>Text in a</a>                                                                |
|     <b>Text in b</b>                                                                |
|     <b>Text in b again</b>                                                          |
|   </test>                                                                           |
|   <test>                                                                            |
|     <a>Text in a</a>                                                                |
|     <b>Text in b</b>                                                                |
|     <c>Text in c</c>                                                                |
|   </test>                                                                           |
|   <test>                                                                            |
|     <a>Text in a</a>                                                                |
|     <b>Text in b</b>                                                                |
|     <d>Text in d</d>                                                                |
|     <e></e>                                                                         |
|   </test>                                                                           |
|   <test>                                                                            |
|     <a>Text in a</a>                                                                |
|     <b>Text in b</b>                                                                |
|     <e/>                                                                            |
|   </test>                                                                           |
|   <test>                                                                            |
|     <a>Text in a</a>                                                                |
|     <b>Text in b</b>                                                                |
|     <f/>                                                                            |
|   </test>                                                                           |
| </top>                                                                              |
|                                                                                     |
| NOTE: some of the elements have the 'ignore' function set to test the feature.      |
|       See the detail of the graph model below.                                      |
|                                                                                     |
|-------------------------------------------------------------------------------------|
| Tested: Ubuntu Precise (12.04 x64)                                                  |
|========                                                                             |
|-------------------------------------------------------------------------------------|
| Contributions:                                                                      |
|===============                                                                      |
|-------------------------------------------------------------------------------------|
| History:                                                                            |
|=========                                                                            |
| 1.0.0 : Initial version                                                             |
|-------------------------------------------------------------------------------------|
| Compiling instructions:                                                             |
|========================                                                             |
|  gcc -Wall -D_GNU_SOURCE -g vc_test.c -c -o vc_test.o                               |
|  gcc vc_test.o validator.o -lexpat -o vc_test                                       |
|-------------------------------------------------------------------------------------|
| Notes:                                                                              |
|=======                                                                              |
| The program takes two parameters.                                                   |
| - 1st parameter is the path to the xml file to validate. If no parameter is passed  |
|   it defaults to vc_test.xml.                                                       |
| - 2nd parameter, when present, triggers the mode "partial validation". The          |
|   validation will start when we encounter the start of the element specified by     |
|   this parameter.                                                                   |
\====================================================================================*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdarg.h>

#include <expat.h>

#include "../validator.h"




struct XMLUserData
  {
    XML_Parser    pXMLParser;
    VC_Validator  pVCValidator;
    XML_Char     *pTopParent;
    XML_Char     *pLastString;
    unsigned int  sLastString;
    unsigned int  depth;
    XML_Bool      fDone;
    unsigned long nChar;
    unsigned long nComment;
    unsigned long nPI;
    enum VC_Error VC_Error;
  };


#define PUSERDATA ((struct XMLUserData *)pUserData)

                             /* Utility functions for indentation and printing  */
                             /* of 'lastString' (when not 'blank')              */
void indent( pUserData )
       void *pUserData;
{
  unsigned int i;
  for (i=0; i< PUSERDATA->depth; i++)
    printf("  ");
}

void printText( pUserData )
          void *pUserData;
{
  XML_Char *p;

  p= PUSERDATA->pLastString;
  if ( p!= NULL )
    {
      int i=0;
      while( p[i] != '\0' )
        {
          if ( ! isspace(p[i]) )
            {
              indent(pUserData);
              printf("['%s']\n",p);
              break;
            }
          i++;
        }
      XML_MemFree( PUSERDATA->pXMLParser, p);
      PUSERDATA->pLastString= NULL;
      PUSERDATA->sLastString= 0;
    }
}

                             /* Callbacks when we are not validating (useful    */
                             /* for 'partial' validation).                      */
void
NoValidation_startElt(  pUserData, el, attr)
    struct XMLUserData *pUserData;
    const char         *el  ;
    const char        **attr;
{
  (void) attr;

  indent(pUserData);
  printf("{Not-Validated-Start %s\n",el);

  if ( pUserData->pTopParent != NULL && strcmp(pUserData->pTopParent, el) == 0 )
    {
      indent(pUserData);
      printf("==> Starting validation from here!\n");
      VC_ValidatorStart( pUserData->pVCValidator );
    }

  PUSERDATA->depth++;
}

void
NoValidation_endElt(   pUserData, el)
    struct XMLUserData *pUserData;
    const char         *el  ;
{
  if ( PUSERDATA->depth )
    PUSERDATA->depth--;
  indent(pUserData);
  printf("Not-Validated-End %s}\n",el);
}


                             /* Callbacks for start/end element/vertex          */
void XMLCALL
Test_StartVx (       pUserData, el, attr )
    void            *pUserData;
    const XML_Char  *el;
    const XML_Char **attr;
{
  VC_Stack *pStack;

  (void) attr;

  printText(pUserData);
  indent(pUserData);
  printf("(start vertex %s ",el);
  pStack= VC_GetCurrentStack( PUSERDATA->pVCValidator );
  if ( pStack->pThis->pCallbacks == NULL ||
       pStack->pThis->pCallbacks->start == NULL
     )
    {
      PUSERDATA->depth++;
      printf("\n");
    }
}

void XMLCALL
Test_StartElt (      pUserData, el, attr )
    void            *pUserData;
    const XML_Char  *el;
    const XML_Char **attr;
{
  VC_Stack *pStack;

  (void) attr;

  pStack= VC_GetCurrentStack( PUSERDATA->pVCValidator );
  if (   pStack->pParent != NULL &&
       ( pStack->pParent->pStep->pCallbacks == NULL ||
         pStack->pParent->pStep->pCallbacks->start == NULL
       )
     )
    {
      printText(pUserData);
      indent(pUserData);
    }
  printf(">>start element %s\n",el);
  PUSERDATA->depth++;
}


void XMLCALL
Test_EndElt (       pUserData, el )
    void           *pUserData;
    const XML_Char *el;
{
  VC_Stack *pStack;

  printText(pUserData);
  PUSERDATA->depth--;
  indent(pUserData);
  printf("end element %s <<",el);
  pStack= VC_GetCurrentStack( PUSERDATA->pVCValidator );
  if (   pStack->pParent != NULL &&
       ( pStack->pParent->pStep->pCallbacks == NULL ||
         pStack->pParent->pStep->pCallbacks->end == NULL
       )
     )
    {
      printf("\n");
    }
}

void XMLCALL
Test_EndVx (        pUserData, el )
    void           *pUserData;
    const XML_Char *el;
{
  VC_Stack *pStack;

  pStack= VC_GetCurrentStack( PUSERDATA->pVCValidator );
  if ( pStack->pThis->pCallbacks == NULL ||
       pStack->pThis->pCallbacks->end == NULL
     )
    {
      printText(pUserData);
      PUSERDATA->depth--;
      indent(pUserData);
    }
  printf(" end vertex %s)\n",el);
}

                             /* Callbacks for text, comment, P.I.               */
void
Test_charhndl(      pUserData, s, len)
    void           *pUserData;
    const XML_Char *s        ;
    int             len      ;
{
  void *newStr;

  PUSERDATA->nChar += len;

  newStr= XML_MemRealloc( PUSERDATA->pXMLParser, 
                          PUSERDATA->pLastString,
                          PUSERDATA->sLastString + len + 1
                        );
  if (newStr==NULL)
    {
      printf("Memory allocation error.\n");
      exit(1);
    }
  else
    PUSERDATA->pLastString= newStr;

  memcpy( PUSERDATA->pLastString + PUSERDATA->sLastString, s, len );
  PUSERDATA->sLastString += len;
  PUSERDATA->pLastString[PUSERDATA->sLastString]='\0';
}

void
Test_CommentHandler(  pUserData, data)
    void             *pUserData;
    const XML_Char   *data     ;
{
  ((struct XMLUserData *)pUserData)->nComment += strlen(data);
}

void
Test_PIHandler (    pUserData, target, data)
    void           *pUserData;
    const XML_Char *target   ;
    const XML_Char *data     ;
{
  (void) target;
  (void) data;

  ((struct XMLUserData *)pUserData)->nPI++;
}
                             /* IMPORTANT: this is the 'normal' way to end a    */
                             /*  partial validation. This callback is attached  */
                             /*  to the end of topmost element. Note that we    */
                             /*  MUST set ALL 5 callbacks: start/end/chr/PI/com */
                             /*  even if some are unused (set them to NULL then)*/
void resetHandlers( pUserData )
    void           *pUserData;
{
  XML_SetElementHandler( PUSERDATA->pXMLParser,
                        (XML_StartElementHandler)NoValidation_startElt,
                        (XML_EndElementHandler)NoValidation_endElt
                       );
  XML_SetProcessingInstructionHandler 
                       ( PUSERDATA->pXMLParser, NULL );
  XML_SetCommentHandler( PUSERDATA->pXMLParser, NULL );
  XML_SetCharacterDataHandler
                       ( PUSERDATA->pXMLParser, NULL );

  PUSERDATA->VC_Error= VC_GetErrorCode(PUSERDATA->pVCValidator);
  VC_ValidatorFree(PUSERDATA->pVCValidator);
  PUSERDATA->fDone= XML_TRUE;
}

void XMLCALL
Last_EndElt (       pUserData, el )
    void           *pUserData;
    const XML_Char *el;
{
  VC_ValidationStatus pStatus;

  Test_EndElt( pUserData, el);
  printf("== Going back to non-validated parsing now!\n");

  VC_GetValidationStatus( PUSERDATA->pVCValidator, &pStatus);
  printf( "Validation status=%u, final callback=%s\n",
           pStatus.validating,
          (pStatus.finalCallback == VC_TRUE) ? "True" : "False"
        );

  resetHandlers( pUserData );
}

void XMLCALL
Continue_EndElt (   pvci, el )
    VC_Validator    pvci;
    const XML_Char *el;
{
  struct XMLUserData *pUserData;

  (void) el;

  printf("<== Going back to non-validated parsing now!\n");

  pUserData= VC_GetUserData(  pvci );
  resetHandlers( pUserData );
}
                             /* Function to ignore errors. It is IMPORTANT to   */
                             /* NOT ignore out of memory (thus the test). If    */
                             /* we want to ignore an error return VC_ERROR_NONE */
enum VC_Error XMLCALL
Ignore_Errors(    error, pUserData, el)
    enum VC_Error error;
    void         *pUserData;
    const char   *el;
{
  VC_ValidationStatus pStatus;

  if (error == VC_ERROR_OUT_OF_MEMORY)
    return error;

  printf("[Ignoring the error=%u:%s", error, VC_ErrorString(error) );
  if (el==NULL)
    printf("]\n");
  else
    {
      printf(" (the element was: %s) ]\n", el);
      if ( strcmp(el,"STOP") == 0 ||
           strcmp(el,"ABORT") == 0 ||
           strcmp(el,"CONTINUE") == 0 )
        {
          VC_GetValidationStatus( PUSERDATA->pVCValidator, &pStatus);
          printf( "Validation status=%u, final callback=%s\n",
                   pStatus.validating,
                  (pStatus.finalCallback == VC_TRUE) ? "True" : "False"
                );
          printf( "Now stopping validator!\n" );
          VC_StopValidator( PUSERDATA->pVCValidator, XML_FALSE ); 
          if ( strcmp(el,"ABORT") == 0 )
            XML_StopParser( PUSERDATA->pXMLParser, XML_FALSE );
          else
            {
              if ( strcmp(el,"CONTINUE") == 0 )
                {
                  XML_SetEndElementHandler( PUSERDATA->pXMLParser,
                                            (XML_EndElementHandler)Continue_EndElt
                                          );
                }
            }

          return VC_ERROR_ABORTED;
        }
    }

  if (error == VC_ERROR_TEXT_NOT_ALLOWED_HERE)
    {
      XML_MemFree( PUSERDATA->pXMLParser, PUSERDATA->pLastString);
      PUSERDATA->pLastString= NULL;
      PUSERDATA->sLastString= 0;
    }

  return VC_ERROR_NONE;
}


                             /* This is the DTD transformed to a graph.         */
                             /* 1st the callbacks used, grouped in categories   */

                             /* This NULL callback is useless as you can just   */
                             /* put a NULL pointer to the callback struct, but  */
                             /* it allows us to test that everything is fine    */
                             /* when we have NULL pointers inside this struct.  */
static VC_Handlers hnd_null      = { NULL         , NULL         , NULL,
                                                    NULL         , NULL,
                                                    NULL          }; 
static VC_Handlers hnd_vtx       = { NULL         , Test_StartVx , Test_EndVx ,
                                                    Test_charhndl, Test_PIHandler,
                                                    Test_CommentHandler }; 
static VC_Handlers hnd_vtx_ignore= { Ignore_Errors, Test_StartVx , Test_EndVx ,
                                                    Test_charhndl, Test_PIHandler,
                                                    Test_CommentHandler }; 
static VC_Handlers hnd_elt       = { NULL         , Test_StartElt, Test_EndElt,
                                                    Test_charhndl, Test_PIHandler,
                                                    Test_CommentHandler }; 
static VC_Handlers hnd_elt_ignore= { Ignore_Errors, Test_StartElt, Test_EndElt,
                                                    Test_charhndl, Test_PIHandler,
                                                    Test_CommentHandler }; 
static VC_Handlers hnd_doc       = { NULL         , NULL         , Last_EndElt,
                                                    Test_charhndl, Test_PIHandler,
                                                    Test_CommentHandler }; 

                             /* And here the graph itself.                      */
static VC_Content b   = { {"b"}, &hnd_null, VC_F_MIXED | VC_F_FINAL, 0, NULL };
static VC_Content c   = { {"c"}, NULL     , VC_F_MIXED | VC_F_FINAL, 0, NULL };
static VC_Content d   = { {"d"}, &hnd_elt , VC_F_MIXED | VC_F_FINAL, 0, NULL };
static VC_Content e   = { {"e"}, &hnd_elt ,              VC_F_FINAL, 0, NULL };
static VC_Content f   = { {"f"}, &hnd_elt_ignore,        VC_F_FINAL, 0, NULL };


static VC_Content a;
static VC_Content a_a;
static VC_Content a_b;
static VC_Content a_c;
static VC_Content a_d;

static VC_Content *p_a_0[4]= { &a_a, &a_b, &a_c, &a_d };

static VC_Content a_a ={ {.element=&a}, &hnd_vtx_ignore, VC_F_FINAL | VC_F_MIXED, VC_EDGE(p_a_0) };
static VC_Content a_b ={ {.element=&b}, &hnd_vtx_ignore, VC_F_FINAL | VC_F_MIXED, VC_EDGE(p_a_0) };
static VC_Content a_c ={ {.element=&c}, NULL           , VC_F_FINAL | VC_F_MIXED, VC_EDGE(p_a_0) };
static VC_Content a_d ={ {.element=&d}, NULL           , VC_F_FINAL | VC_F_MIXED, VC_EDGE(p_a_0) };

static VC_Content a   = { {"a"}, &hnd_elt_ignore , VC_F_FINAL | VC_F_MIXED, VC_EDGE(p_a_0) };


static VC_Content test_a;
static VC_Content test_b;
static VC_Content test_c;
static VC_Content test_d;
static VC_Content test_e;
static VC_Content test_f;

static VC_Content *p_test_0[1]= { &test_a };
static VC_Content *p_test_a[5]= { &test_a, &test_b, &test_d, &test_e, &test_f };
static VC_Content *p_test_b[6]= { &test_a, &test_b, &test_c, &test_d, &test_e, &test_f };
static VC_Content *p_test_d[2]= { &test_d, &test_e };

static VC_Content test_a ={ {.element=&a}, &hnd_vtx, VC_F_FINAL, VC_EDGE(p_test_a) };
static VC_Content test_b ={ {.element=&b}, &hnd_vtx, VC_F_FINAL, VC_EDGE(p_test_b) };
static VC_Content test_c ={ {.element=&c}, &hnd_vtx, VC_F_FINAL, VC_EDGE(p_test_a) };
static VC_Content test_d ={ {.element=&d}, &hnd_vtx, 0         , VC_EDGE(p_test_d) };
static VC_Content test_e ={ {.element=&e}, &hnd_vtx, VC_F_FINAL, VC_EDGE(p_test_a) };
static VC_Content test_f ={ {.element=&f}, &hnd_vtx, VC_F_FINAL, VC_EDGE(p_test_0) };

static VC_Content test = { {"test"}, &hnd_elt_ignore, 0, VC_EDGE(p_test_0) };


static VC_Content top_test;
static VC_Content *p_top_0[1]= { &top_test };
static VC_Content top_test ={ {.element= &test}, &hnd_vtx, VC_F_FINAL, VC_EDGE(p_top_0) };
static VC_Content top =  { { "top" }, &hnd_elt , 0, VC_EDGE(p_top_0) };


static VC_Content doc_top ={ { .element= &top}, &hnd_vtx, VC_F_FINAL, 0, NULL };
static VC_Content *p_doc_0[1]= { &doc_top };
static VC_Content doc = { {NULL}, &hnd_doc, 0, VC_EDGE(p_doc_0) };



int
main(  argc, argv)
 int   argc;
 char *argv[];
{
  struct XMLUserData XMLUserData={NULL, NULL, NULL, NULL, 0, 0, XML_FALSE, 0, 0, 0, VC_ERROR_NONE};
  FILE *fp;
  size_t l;
  int    final;
  enum XML_Status XML_Status;
  VC_ValidationStatus pStatus;
  char   buffer[512];
  char  *pFileName = "vc_test.xml";

  if (argc>1)                /* 1st argument is the XML file                    */
    {                        /* if none given, it defaults to vc_test.xml       */
      pFileName=argv[1];
    }

  if (argc>2)                /* 2nd argument is the element embedding our XML   */
    {                        /* as defined by the DTD. Defaut=document node     */
      XMLUserData.pTopParent=argv[2];
    }

  fp= fopen(pFileName,"r");
  if(fp)
    {

                             /* We create the parser and the validator          */
      XMLUserData.pXMLParser= XML_ParserCreateNS(NULL,'\0');
      if (XMLUserData.pXMLParser == NULL)
        {
          printf("Error creating the parser\n");
          return 1;
        }
      XML_SetUserData(XMLUserData.pXMLParser, &XMLUserData);
      XMLUserData.pVCValidator= VC_ValidatorCreate( XMLUserData.pXMLParser, &doc );
      if (XMLUserData.pVCValidator == NULL)
        {
          printf("Error creating the validator\n");
          return 1;
        }

                             /* If all OK, printing the header to show what we  */
                             /* are doing (feedback on arguments)               */
      printf("Starting validation for file: %s ", pFileName);
      if (XMLUserData.pTopParent == NULL)
        printf ( "at document node" );
      else
        printf ( "from %s element", XMLUserData.pTopParent );
      printf(", with %s version.\n", VC_ExpatValidatorVersion() );

      if ( XMLUserData.pTopParent == NULL )
        {                    /* Here we validate the whole document             */
          VC_ValidatorStart( XMLUserData.pVCValidator );
        }
      else
        {                    /* and only part here, so we start handlers        */
          XML_SetElementHandler( XMLUserData.pXMLParser,
                                (XML_StartElementHandler)NoValidation_startElt,
                                (XML_EndElementHandler)NoValidation_endElt
                               );
        }

                             /* Now we do the parsing. No difference from a     */
                             /* regular expat parsing.                          */
      do
        {
          l= fread( buffer, sizeof(char), sizeof(buffer), fp );
          final= ( l < sizeof(buffer) * sizeof(char) );
          XML_Status= XML_Parse( XMLUserData.pXMLParser, buffer, l, final );
        }
      while(!final && XML_Status== XML_STATUS_OK);
      fclose(fp);

                             /* The flag fDone is set when partial validation   */
                             /* was OK, thus we fetch VC_ERROR only when the    */
                             /* flag is unset.                                  */
      if (XMLUserData.fDone== XML_FALSE)
        XMLUserData.VC_Error= VC_GetErrorCode(XMLUserData.pVCValidator);

                             /* Display final status and results.               */
      if ( XMLUserData.VC_Error == VC_ERROR_NONE )
        {
          printf("XML was valid\n");
          printf("%lu chars in text nodes\n",XMLUserData.nChar);
          printf("%lu chars in comment nodes\n",XMLUserData.nComment);
          printf("%lu processing instructions\n",XMLUserData.nPI);
        }
      else
        printf("Invalid XML: %s\n", VC_ErrorString(XMLUserData.VC_Error));

      if (XML_Status== XML_STATUS_OK)
        printf("All is OK\n");
      else
        printf( "Parser error %u: %s\nAt line: %lu, column: %lu\n",
                XML_GetErrorCode(XMLUserData.pXMLParser),
                XML_ErrorString(XML_GetErrorCode(XMLUserData.pXMLParser)),
                XML_GetCurrentLineNumber(XMLUserData.pXMLParser),
                XML_GetCurrentColumnNumber(XMLUserData.pXMLParser)
              );

                             /* Same here, we don't free if the flag is true as */
                             /* there was a successful partial validation that  */
                             /* already freed the validator.                    */
      if (XMLUserData.fDone== XML_FALSE)
        {
          VC_GetValidationStatus( XMLUserData.pVCValidator, &pStatus);
          printf( "Validation status=%u, final callback=%s\n",
                   pStatus.validating,
                  (pStatus.finalCallback == VC_TRUE) ? "True" : "False"
                );

          VC_ValidatorFree(XMLUserData.pVCValidator);
        }

      XML_MemFree( XMLUserData.pXMLParser, XMLUserData.pLastString);

      XML_ParserFree(XMLUserData.pXMLParser);
    }
  else
    {
      printf("Error opening file %s\n",pFileName);
    }

  return 0;
}

