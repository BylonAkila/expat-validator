#! /bin/sh

# This script tests the validator against the available use case.
#
# You must first run vc_make_result.sh to build the expected results.
# This should be done with the reference version of vc_test
#
# Once this is done, you can do your own modifications in the validator
# then run this script again to check you didn't introduce any regression.
#
# If you have Valgrind installed, the script will run the test cases
# through Valgrind and check that there is no memory leak.
#
# The files named *.xml are validated entirely, with *.xml2 we do a
# partial validation starting when we encounter the element <start>

mkdir /tmp/vc_test 2>/dev/null


valgrind="$( which valgrind )"
memok='All heap blocks were freed -- no leaks are possible'

if [ -n "$valgrind" ]; then
  echo "Testing with valgrind"
fi

for f in *.xml
  do
    if [ "$f" != '*.xml' ];
      then
        echo -n "Testing $f ... "
        if [ -f "results/$f.txt" ]; then
          if [ -n "$valgrind" ]; then
            "$valgrind" ./vc_test "$f" 1>"/tmp/vc_test/$f" 2>"/tmp/vc_test/err_${f}"
          else
            ./vc_test "$f" 1>"/tmp/vc_test/$f" 2>"/tmp/vc_test/err_${f}"
          fi
          if diff -q "/tmp/vc_test/$f" "results/$f.txt" >/dev/null 2>/dev/null; then
            echo -n "OK"
          else
            echo -n "FAILED!"
          fi
          if [ -n "$valgrind" ]; then
            if grep -q "$memok" "/tmp/vc_test/err_${f}"; then
              echo " (no memory leak)"
            else
              echo " ATTENTION: memory leak!"
            fi
          else
            echo ''
          fi
          rm "/tmp/vc_test/$f" "/tmp/vc_test/err_${f}"
        else
          echo "not found in the result-set!"
        fi
      fi
  done


for f in *.xml2
  do
    if [ "$f" != '*.xml2' ];
      then
        echo -n "Testing $f ... "
        if [ -f "results/$f.txt" ]; then
          if [ -n "$valgrind" ]; then
            "$valgrind" ./vc_test "$f" start 1>"/tmp/vc_test/$f" 2>"/tmp/vc_test/err_${f}"
          else
            ./vc_test "$f" start 1>"/tmp/vc_test/$f" 2>"/tmp/vc_test/err_${f}"
          fi
          if diff -q "/tmp/vc_test/$f" "results/$f.txt" >/dev/null 2>/dev/null; then
            echo -n "OK"
          else
            echo -n "FAILED!"
          fi
          if [ -n "$valgrind" ]; then
            if grep -q "$memok" "/tmp/vc_test/err_${f}"; then
              echo " (no memory leak)"
            else
              echo " ATTENTION: memory leak!"
            fi
          else
            echo ''
          fi
          rm "/tmp/vc_test/$f" "/tmp/vc_test/err_${f}"
        else
          echo "not found in the result-set!"
        fi
      fi
  done


rmdir /tmp/vc_test 2>/dev/null
