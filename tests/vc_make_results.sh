#! /bin/sh

# This script tests the validator against the available use case.
#
# It builds build the expected results and should be run with the reference
# version of vc_test. You can run it again when you add test cases.
# The expected output is stored under the results directory with the
# name of the test-case suffixed with .txt
#
# Once this is done, you can do your own modifications in the validator
# then run the other script (vc_test.sh) to check you didn't introduce any
# regression.
#
# If you have Valgrind installed, this script will run the test cases
# through Valgrind and check that there is no memory leak.
#
# The files named *.xml are validated entirely, with *.xml2 we do a
# partial validation starting when we encounter the element <start>


rm results/*.xml.txt 2>/dev/null

valgrind="$( which valgrind )"
memok='All heap blocks were freed -- no leaks are possible'

if [ -n "$valgrind" ]; then
  echo "Checking with valgrind"
fi

for f in *.xml
  do
    if [ "$f" != '*.xml' ];
      then
        echo -n "Making result for $f ... "
        if [ -n "$valgrind" ]; then
          "$valgrind" ./vc_test "$f" 1>"results/$f.txt" 2>"/tmp/vc_test_err_${f}"
          if grep -q "$memok" "/tmp/vc_test_err_${f}"; then
            echo -n "(no memory leak) ... "
          else
            echo -n "ATTENTION: memory leak! ... "
          fi
          rm "/tmp/vc_test_err_${f}"
        else
          ./vc_test "$f" >"results/$f.txt"
        fi
        echo "DONE"
      fi
  done

for f in *.xml2
  do
    if [ "$f" != '*.xml2' ];
      then
        echo -n "Making result for $f ... "
        if [ -n "$valgrind" ]; then
          "$valgrind" ./vc_test "$f" start 1>"results/$f.txt" 2>"/tmp/vc_test_err_${f}"
          if grep -q "$memok" "/tmp/vc_test_err_${f}"; then
            echo -n "(no memory leak) ... "
          else
            echo -n "ATTENTION: memory leak! ... "
          fi
          rm "/tmp/vc_test_err_${f}"
        else
          ./vc_test "$f" start >"results/$f.txt"
        fi
        echo "DONE"
      fi
  done



