/*=====================================================================
 Copyright (C) 2012 Alain BENEDETTI aka Zakhar @ ubuntu.fr

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published byr
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>
=====================================================================*/

/*====================================================================================\
| Date: 2012-10-21                                                                    |
|======                                                                               |
|-------------------------------------------------------------------------------------|
| Author: Alain BENEDETTI aka Zakhar                                                  |
|========                                                                             |
|-------------------------------------------------------------------------------------|
| Version: 1.0.0                                                                      |
|=========                                                                            |
|-------------------------------------------------------------------------------------|
| Function:                                                                           |
|==========                                                                           |
| This generic VC (Validation Constraint) over Expat. As I needed that for WebDAV,    |
| there are some limitations, the most important one being that attributes validation |
| are not implemented (not needed in, the WebDAV DTD).                                |
|                                                                                     |
| The other big limitation is that it does not validate from a DTD but from a "graph" |
| which is the internal model we use. For dynamic DTD validation, we would have to    |
| first transform the DTD into a "graph".                                             |
|                                                                                     |
| The "graph" model used simplifies the validation process to a trivial string search.|
| The validation process, for elements, is mainly: what start element is allowed      |
| where we are (as 1st child of a parent, or as sibling), and for end element, is the |
| end of the element allowed here. The "graph" represents exactly that, for contents  |
| like: name, sequence or choice, we divide the type declaration into "vertices",     |
| these vertices are linked by "edges" which represent the possible next sibling (or  |
| 1st child when we are at the start of an element's content).                        |
| Let's illustrate that with:                                                         |
| <!ELEMENT test ( a, ( b | (c*, b, a?) ) ) >                                         |
| We have 5 vertices, simply obtained by counting the names inside the declaration:   |
| a (the 1st one), b, c, b (agin, the seond one), a (again, the second one).          |
| IMPORTANT: note that, althouh they have almost the same structure, we must not      |
| confuse vertices and elements. A given element can appear several times in a        |
| declaration, if so it must be modelled with as many vertices as it is repeated.     |
| The "Edges", then define what element can appear after a given one. But similar to  |
| the pole and fences issue, of course, when we have N vertices, we have N+1 edges.   |
| In fact, the 1st edges belongs to the element declaration, and defines which        |
| first children are valid.                                                           |
| In our example:                                                                     |
| -Edge_0 (contained in element: which 1st child in this element): only 1: a          |
| -Edge_1 (contained in the vertex a, 1st one): next siblings 3: b, c or second b     |
| -Edge_2 (contained in vertex b): next sibling 0, it means the element MUST be ended |
| -Edge_3 (contained in vertex c): next sibling 2: c (repeated) or second b           |
| -Edge_4 (contained in vertex second b): next sibling 1: a, BUT: element CAN end now |
| -Edge_5 (contained in vertex second a): next sibling 0: same as b, MUST end element |
|                                                                                     |
| Thus, to cope with compulsary elements, the edge definition has a flag that says if |
| the element is allowed (MUST or CAN) to be ended after the current vertex.          |
| We do also have another flag for "mixed" and "any" that indicates if text nodes     |
| are allowed to appear here.                                                         |
|                                                                                     |
| This model is sufficient to translate all DTDs for content validation purpose, with |
| the limitation that we did not implement attributes!                                |
| The validation process is then extremely simple, from one element, we know which    |
| element can appear next (as first child or as next sibling), just by following      |
| the edges (from the element, or from the current vertex).                           |
| When we have an end tag, we just need to check if we were allowed to have that      |
| with the flag (for end tag, we don't even need to do a string compare, because      |
| if the strings don't match it is a "well-formed" error and the parser would stop).  |
| For text nodes, it is allowed when the flag is up. On elements with children (not   |
| EMPTY), they are allowed if it is only "spacing".                                   |
| Comments and PI are always allowed except inside an EMPTY element (with 0 child).   |
|-------------------------------------------------------------------------------------|
| Tested: Ubuntu Precise (12.04 x64)                                                  |
|========                                                                             |
|-------------------------------------------------------------------------------------|
| Contributions:                                                                      |
|===============                                                                      |
|-------------------------------------------------------------------------------------|
| History:                                                                            |
|=========                                                                            |
| 1.0.0 : Initial version                                                             |
| 1.0.1 : Cleaning to pass more warning checks: -Wextra -std=c1x -pedantic            |
| 1.0.2 : Removed deprecated K&R style non-prototype function definition              |
|-------------------------------------------------------------------------------------|
| Compiling instructions:                                                             |
|========================                                                             |
|  gcc -Wall -Wextra -D_GNU_SOURCE -std=c1x -pedantic validator.c -c -o validator.o   |
| of course, you add -g or -O3 if you want repectively to debug, a fast version.      |
| Makefile compiles with -O3                                                          |
|-------------------------------------------------------------------------------------|
| Notes:                                                                              |
|=======                                                                              |
| IMPORTANT: to optimise the validation process, the start element routine does a     |
| bsearch. Thus it is ESSENTIAL that the possible end vertices of an edge are SORTED  |
| by ascending name of the element.                                                   |
|                                                                                     |
| IMPORTANT: the validator acts as a "filter" between the program that uses it and    |
| the underlying expat. Thus, when using the validator, the application MUST NOT      |
| set StartElementHandler, EndElementHandler, ProcessingInstructionHandler,           |
| CommentHandler or CharacterHandler. Instead, such functions MUST be attached to the |
| graph model describing the DTD, and they will be called when the validator          |
| encounters the relevant element.                                                    |
| If a callback handler wants to change or get the UserData structure, instead of     |
| calling XML_Get/SetUserData, it must call its counterpart VC_Get/SetUserData.       |
| There are also 2 functions equivalent to the XML_ErrorCode, XML_ErrorMsg, prefixed  |
| by VC_, to get the validation error code and message. The error codes do not        |
| overlap with those of expat.                                                        |
|                                                                                     |
|-------------------------------------------------------------------------------------|
| Convention:                                                                         |
|============                                                                         |
| The structures and functions we use internally are prefixed with _vc_ they are not  |
| meant to be used/called directly by the program using our validator.                |
| The exposed functions and structures are prefixed by VC_, same as expat prefixes    |
| by XML_. They are also in the header definition, and calling application SHOULD     |
| use/call only those exposed elements.                                               |
|-------------------------------------------------------------------------------------|
| TODO:                                                                               |
| - Implement attribute validation.                                                   |
| - Test in the case that XML_Char, XML_LChar are not bytes.                          |
\====================================================================================*/
/*====================================================================================\
| Page: 1  |                                                                          |
|==========/                                                                          |
|  SUMMARY:                                                                     PAGE  |
|  --------                                           ______________________    ----  |
|  __________________________________________________/ SECTION: 'internals' \________ |
|  Prologue (defines, types, constants, etc) .................................     2  |
|  _vc_error() ...............................................................     3  |
|  _vc_namecomp(), _vc_startElt() ............................................     4  |
|  _vc_endElt() ..............................................................     5  |
|  _vc_PIHandler(), _vc_CommentHandler() .....................................     6  |
|  _vc_charhndl() ............................................................     7  |
|                                                                                     |
|                                              _____________________________          |
|  ___________________________________________/ SECTION: 'Public' functions \________ |
|  VC_ValidatorCreate() ......................................................    10  | 
|  VC_ValidatorStart() .......................................................    11  |
|  VC_ValidatorFree() ........................................................    12  |
|  VC_SetUserData(), VC_GetUserData(), VC_UseValidatorAsHandlerArg() .........    13  |
|  VC_GetErrorCode(), VC_ErrorString(), VC_ExpatValidatorVersion() ...........    14  |
|  VC_StopValidator(), VC_GetValidationStatus() ..............................    15  |
|  VC_GetCurrentStack() ......................................................    16  |
\====================================================================================*/
/*====================================================================================\
| Page: 2  |                                                                          |
|==========/                                                                          |
| Description:                                                                        |
|=============                                                                        |
| Includes and internal structures and constants.                                     |
\====================================================================================*/
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stddef.h>
#include <expat.h>

#include "validator.h"


struct _vc_Internal
{
  VC_Stack          *pStack      ;
  XML_Parser         p           ;
  void              *psvgUserData;
  void              *pXMLUserData;
  enum VC_Error      errorCode   ;
  unsigned int       ignoreDepth ;
  enum VC_Validating status      ;
  unsigned int       flags       ;
};

#define _vc_F_LAST_CALLBACK  1
#define _vc_F_FREE_REQUESTED 2

static const XML_LChar *pVC_ErrorStrings[VC_ERROR_INTERNAL_ERROR - VC_ERROR_NONE + 2]= 
  {
    "No error: XML is valid",       /* VC_ERROR_NONE (must be the first element)     */
    "Element not allowed here",     /* VC_ERROR_ELEMENT_NOT_ALLOWED_HERE             */
    "Element ended prematurely",    /* VC_ERROR_ELEMENT_ENDED_PREMATURELY            */
    "Text not allowed here",        /* VC_ERROR_TEXT_NOT_ALLOWED_HERE                */
                                    /* VC_ERROR_PI_NOT_ALLOWED_IN_EMPTY_ELEMENT      */
    "Processing Intruction not allowed in empty element",
                                    /* VC_ERROR_COMMENT_NOT_ALLOWED_IN_EMPTY_ELEMENT */
    "Comment not allowed in empty element",
    "Out of memory",                /* VC_ERROR_OUT_OF_MEMORY_ELEMENT                */
    "Validation aborted",           /* VC_ERROR_ABORTED                              */
                                    /* VC_ERROR_ALREADY_FINISHED                     */
    "Validation was already finished",
                                    /* VC_ERROR_INTERNAL_ERROR (must be before last) */
    "Error retrieving internal buffer",
                                    /* This must be the last element                 */
    "Error code out of bounds"
  };

/*====================================================================================\
| Page: 3  |                                                                          |
|==========/                                                                          |
| Function: _vc_error()                                                               |
|==========                                                                           |
|                                                                                     |
| Description:                                                                        |
|=============                                                                        |
| Internal utility functions.                                                         |
|                                                                                     |
| _vc_error is called whenever a validation error is encountered.                     |
|  It calls the user defined error handler function, passing it the error code, the   |
|  userdata and the element (for start/end tag). If the user error function decides   |
|  to ignore the error, it must return VC_ERROR_NONE, otherwise it returns a valid    |
|  error code. NOTE: it is unsafe, and will lead to unpredictable results, to         |
|  ignore an 'out of memory' error.                                                   |
|  If there was no user function, the default behaviour is to report the error then   |
|  stop the parsing.                                                                  |
|  To be safe, when an error was reported (directly or through a user handler that    |
|  did not ignore the error) we flag the validation process as ended.                 |
\====================================================================================*/

static enum VC_Error
_vc_error( struct _vc_Internal *pvci   , 
           enum VC_Error        errCode,
           const char          *el       )
{
  if ( pvci->pStack->pStep->pCallbacks        != NULL &&
       pvci->pStack->pStep->pCallbacks->error != NULL )
    {
      pvci->errorCode= pvci->pStack->pStep->pCallbacks->error( errCode,
                                                               pvci->pXMLUserData,
                                                               el
                                                             );
    }
  else
    {
      pvci->errorCode= errCode;
      XML_StopParser( pvci->p, XML_FALSE );
    }

  if (pvci->errorCode != VC_ERROR_NONE)
    {
      pvci->status = VC_FINISHED;
    }

  return pvci->errorCode;
}


/*====================================================================================\
| Page: 4  |                                                                          |
|==========/                                                                          |
| Function: _vc_namecomp(), _vc_startElt()                                            |
|==========                                                                           |
|                                                                                     |
| Description:                                                                        |
|=============                                                                        |
| This internal function validates that the current element can appear at the current |
| position.                                                                           |
|                                                                                     |
| If we are already ignoring a previous validation error, we just increment the       |
| depth as this is a new child of the ignored element.                                |
| Otherwise, we try to see if that was allowed here looking through the edges of the  |
| current step that gives us all the possible vertices here.                          |
| If the search didn't match, it means the element should not appear here. So we call |
| the internal error function. If it returns VC_ERROR_NONE, it means that there was a |
| user defined error function that instructed us to ignore the error, thus we start   |
| ignoring this element and its content. Otherwise we just return as the error        |
| function will have done the necessary to stop further validation or parsing.        | 
| If the element is legitimate, we allocated a new stack element, initialise it,      |
| stack it, and call the user functions (when there are some). First we call the      |
| vertex function, then the new element function.                                     |
|                                                                                     |
| _vc_namecomp is the internal function used to compare elements strings with our     |
| "graphe model" (used in bsearch).                                                   |
\====================================================================================*/

static int
_vc_namecomp( const void *el,
              const void *pVertex )
{
  return  strcmp(el, (*((VC_Content **)pVertex))->p.element->p.name);
}


static void
_vc_startElt( struct _vc_Internal *pvci, 
              const char          *el  ,
              const char         **attr  )
{
  VC_Content  **ppNext;
  VC_Stack     *pNew  ;
  
  if (pvci==NULL || pvci->status != VC_VALIDATING)
    return;

  if (pvci->ignoreDepth > 0)
    {
      pvci->ignoreDepth++;
    }
  else
    {
      ppNext= bsearch( el,
                       pvci->pStack->pStep->ppVertex,
                       pvci->pStack->pStep->nEdges,
                       sizeof(VC_Content *),
                       _vc_namecomp
                     );
      if ( ppNext == NULL )
        {
          if ( _vc_error( pvci, VC_ERROR_ELEMENT_NOT_ALLOWED_HERE, el ) == 
               VC_ERROR_NONE
             )
            {
              pvci->ignoreDepth++;
            }
          return;
        }

      pNew= XML_MemMalloc( pvci->p, sizeof( VC_Content ) );
      if (pNew != NULL)
        {
          pvci->pStack->pStep = *ppNext                  ;
          pNew->pParent       = pvci->pStack             ;
          pvci->pStack        = pNew                     ;
          pNew->pThis         =
          pNew->pStep         = (*ppNext)->p.element     ;
          if ((*ppNext)->pCallbacks        != NULL && 
              (*ppNext)->pCallbacks->start != NULL )
            {
              (*ppNext)->pCallbacks->start(pvci->pXMLUserData, el, attr);
            }
          if (pNew->pThis->pCallbacks        != NULL &&
              pNew->pThis->pCallbacks->start != NULL )
            {
              pNew->pThis->pCallbacks->start(pvci->pXMLUserData, el, attr);
            }
        }
      else
        {
          _vc_error( pvci, VC_ERROR_OUT_OF_MEMORY, el );
        }
    }
}


/*====================================================================================\
| Page: 5  |                                                                          |
|==========/                                                                          |
| Function: _vc_endElt()                                                              |
|==========                                                                           |
|                                                                                     |
| Description:                                                                        |
|=============                                                                        |
| This internal function validates that we are allowed to end this element now.       |
|                                                                                     |
| It is quite symetrical to the startElement function above, but a little bit simpler |
| as we don't even need look at the element's name. As noted in the general notes in  |
| the header of this file, the element name IS necessarily the one we last saw in     |
| start, otherwise there would be a "well-formed" error and the parsing would have    |
| stopped. The only exception to that is when we are ignoring validation errors,      |
| in which case we just need to count the depth.                                      |
| That is the first thing we do then!                                                 |
| After that we just need to test if we had the flag allowing this element to end.    |
| If it was NOT OK, we call the error function, that will pass the error to the user  |
| defined error handler (if any). If the error is ignored we do as if it was OK (see  |
| below) otherwise we simply return having marked the error.                          |
| If this was OK (or error was ignored), we call the user functions in the reverse    |
| order from the start, thus element end, then vertex end. Then we remove the ended   |
| element (pOld). Unlike startElement, we do these call even when ignoring errors,    |
| because we have an element that ended and we are back in an existing Vertex. We     |
| couldn't do so in start, because it could be an element that is nowhere in our      |
| model, thus we don't know what to call!                                             |
| Then we unstack this element (when OK or ignoring).                                 |
|                                                                                     |
| SPECIAL CASE: when we are back at our top element, there is no vertex to call.      |
| Furthermore, we handle the 'normal way out' which is to call VC_ValidatorFree in    |
| the top element end-handler. Then VC_ValidatorFree kwnows through a flag  that we   |
| are in this situtation, and just marks that Freeing is wanted. The user callbacks   |
| are done normally, and the VC_ValidatorFree is called at the end of this function   |
| when we don't need any of the VC structure, so that we don't trigger a GP Fault!    |
\====================================================================================*/

static void
_vc_endElt( struct _vc_Internal *pvci,
            const char          *el    )
{
  VC_Stack    *pOld;

  if (pvci==NULL || pvci->status != VC_VALIDATING)
    return;

  if (pvci->ignoreDepth > 0)
    {
      pvci->ignoreDepth--;
    }
  else
    {
      if ( (pvci->pStack->pStep->flags & VC_F_FINAL) != VC_F_FINAL)
        {
          if ( _vc_error( pvci, VC_ERROR_ELEMENT_ENDED_PREMATURELY, el ) !=
               VC_ERROR_NONE
             )
            {
              return;
            }
        }

      pOld= pvci->pStack;
      if (pOld->pThis->pCallbacks      != NULL &&
          pOld->pThis->pCallbacks->end != NULL )
        {
          if (pOld->pParent == NULL)
            pvci->flags = _vc_F_LAST_CALLBACK;
          pOld->pThis->pCallbacks->end(pvci->pXMLUserData, el);
        }
      if (pOld->pParent                         != NULL && 
          pOld->pParent->pStep->pCallbacks      != NULL &&
          pOld->pParent->pStep->pCallbacks->end != NULL )
        {
          pOld->pParent->pStep->pCallbacks->end(pvci->pXMLUserData, el);
        }
      pvci->pStack= pOld->pParent;
      XML_MemFree(pvci->p, pOld);

      if (pvci->pStack == NULL)
        {
          pvci->status = VC_FINISHED;
          if (pvci->flags & _vc_F_FREE_REQUESTED)
            {
              VC_ValidatorFree(pvci);
            }
        }
    }
}



/*====================================================================================\
| Page: 6  |                                                                          |
|==========/                                                                          |
| Function: _vc_PIHandler(), _vc_CommentHandler()                                     |
|                                                                                     |
| Description:                                                                        |
|=============                                                                        |
| This internal functions handle processing instructions and comment.                 |
|                                                                                     |
| Basically, the only place where those are forbidden are empty elements, thus we     |
| test just that and call the error routine if necessary. The element's name passed   |
| to the error routine is the one of the should-have-been-empty parent.               |
|                                                                                     |
| Note that we skip all the handler, including custom calls when ignoring.            |
| Unless ignoring, the custom call is always called (if any), be there an validation  |
| error or not.                                                                       |
\====================================================================================*/

static void
_vc_PIHandler ( struct _vc_Internal *pvci  ,
                const XML_Char      *target,
                const XML_Char      *data    )
{
  if (pvci==NULL || pvci->status != VC_VALIDATING || pvci->ignoreDepth > 0)
    return;

  if (pvci->pStack->pStep->pCallbacks     != NULL &&
      pvci->pStack->pStep->pCallbacks->pi != NULL )
    {
      pvci->pStack->pStep->pCallbacks->pi(pvci->pXMLUserData, target, data);
    }

  if (  pvci->pStack->pThis->nEdges              == 0 && 
       (pvci->pStack->pThis->flags & VC_F_MIXED) == 0
     )
    {
      _vc_error( pvci,
                 VC_ERROR_PI_NOT_ALLOWED_IN_EMPTY_ELEMENT,
                 pvci->pStack->pThis->p.name
               );
    }
}


static void
_vc_CommentHandler ( struct _vc_Internal *pvci, 
                     const XML_Char      *data  )
{
  if (pvci==NULL || pvci->status != VC_VALIDATING || pvci->ignoreDepth > 0)
    return;

  if (pvci->pStack->pStep->pCallbacks          != NULL &&
      pvci->pStack->pStep->pCallbacks->comment != NULL )
    {
      pvci->pStack->pStep->pCallbacks->comment(pvci->pXMLUserData, data);
    }

  if (  pvci->pStack->pThis->nEdges              == 0 && 
       (pvci->pStack->pThis->flags & VC_F_MIXED) == 0
     )
    {
      _vc_error( pvci,
                 VC_ERROR_COMMENT_NOT_ALLOWED_IN_EMPTY_ELEMENT,
                 pvci->pStack->pThis->p.name
               );
    }
}


/*====================================================================================\
| Page: 7  |                                                                          |
|==========/                                                                          |
| Function: _vc_charhndl()                                                            |
|==========                                                                           |
|                                                                                     |
| Description:                                                                        |
|=============                                                                        |
| This internal function handles text nodes.                                          |
|                                                                                     |
| We always call the user function if any, even when text is not allowed (if user     |
| provided a function where text is not allowed, he probably want it to be called!).  |
| Then if it is not a MIXED step, if we have some edges in the element, it means the  |
| only text allowed is 'space', and we test for that.                                 |
| On the contrary, if we have no edges in the element and it is not MIXED, it means,  |
| according to our model, that the element is EMPTY, thus text is ALWAYS an error.    |
\====================================================================================*/

static void
_vc_charhndl( struct _vc_Internal *pvci,
              const XML_Char      *s   ,
              int                  len   )
{
  int i;

  if (pvci==NULL || pvci->status != VC_VALIDATING || pvci->ignoreDepth > 0)
    return;

  if (pvci->pStack->pStep->pCallbacks       != NULL &&
      pvci->pStack->pStep->pCallbacks->text != NULL )
    {
      pvci->pStack->pStep->pCallbacks->text(pvci->pXMLUserData, s, len);
    }

  if ( pvci->pStack->pStep->flags & VC_F_MIXED )
    return;

  if ( pvci->pStack->pThis->nEdges != 0 )
    {
      for(i=0; i<len; i++)
        {
          if ( ! isspace(s[i]) ) 
            break;
        }
      if ( i==len )
        {
          return;
        }
    }


  _vc_error( pvci, VC_ERROR_TEXT_NOT_ALLOWED_HERE, NULL );
}


/*====================================================================================\
| Section  |                                                                          |
|==========/                                                                          |
|                                                                                     |
| We finished the internal stuff, now we have the functions exposed to the user's code|
|                                                                                     |
\====================================================================================*/


/*====================================================================================\
| Page: 10 |                                                                          |
|==========/                                                                          |
| Function: VC_ValidatorCreate()                                                      |
|==========                                                                           |
|                                                                                     |
| Description:                                                                        |
|=============                                                                        |
| This function creates and initialises a Validator.                                  |
| Arguments: the parser (returned by XML_ParserCreate for example) and the pointer    |
| on the topmost node of the "graph model".                                           |
| Note that this is usually the document node.                                        |
| It could be any other node, the only trick then is to continue useful parsing after |
| the validation ended. See comment on VC_ValidationFree for that.                    |
|                                                                                     |
| On success, it returns the address of the opaque structure (VC_Validator) we use    |
| internally.                                                                         |
| On failure, it returns NULL.                                                        |
| Note that this function only does the allocation and initialisation, but does not   |
| actually start validating. To do so, you must call the next function:               |
| VC_ValidationStart.                                                                 |
\====================================================================================*/

extern VC_Validator XMLCALL
VC_ValidatorCreate( XML_Parser    p,
                    VC_Content   *pTopElement )
{
  struct _vc_Internal *pvci;

  if (p==NULL || pTopElement==NULL)
    return NULL;

  pvci= XML_MemMalloc( p, sizeof(struct _vc_Internal) );

  if (pvci==NULL)
    return NULL;

  pvci->pStack= XML_MemMalloc( p, sizeof(VC_Stack) );
  if (pvci->pStack==NULL)
    {
      XML_MemFree( p, pvci );
      return NULL;
    }
  pvci->psvgUserData     = 
  pvci->pXMLUserData     = NULL;
  pvci->p                = p;
  pvci->errorCode        = VC_ERROR_NONE;
  pvci->ignoreDepth      = 0;
  pvci->status           = VC_INITIALIZED;
  pvci->flags            = 0;
  pvci->pStack->pParent  = NULL;
  pvci->pStack->pThis    = 
  pvci->pStack->pStep    = pTopElement;

  return pvci;
}


/*====================================================================================\
| Page: 11 |                                                                          |
|==========/                                                                          |
| Function: VC_ValidatorStart()                                                       |
|==========                                                                           |
|                                                                                     |
| Description:                                                                        |
|=============                                                                        |
| This function starts the validation process.                                        |
| Arguments: the pointer returned by a successful call to VC_ValidatorCreate          |
|                                                                                     |
| From here, this function will set the handlers (callbacks) to do the validation.    |
| The userdata is saved and will be restored when freeing.                            |
| If the userdata for the validation was NOT already set by a call to VC_SetUserData  |
| it will be set equal to the current userdata.                                       |
\====================================================================================*/

extern void XMLCALL
VC_ValidatorStart( VC_Validator pvci )
{
  if (pvci != NULL && pvci->status == VC_INITIALIZED)
    {
      XML_SetElementHandler (  pvci->p,
                               (XML_StartElementHandler)_vc_startElt,
                               (XML_EndElementHandler)_vc_endElt
                            );
      XML_SetProcessingInstructionHandler 
                            (  pvci->p,
                               (XML_ProcessingInstructionHandler)_vc_PIHandler
                            );
      XML_SetCommentHandler (  pvci->p,
                               (XML_CommentHandler)_vc_CommentHandler
                            );
      XML_SetCharacterDataHandler(  pvci->p, 
                                   (XML_CharacterDataHandler)_vc_charhndl
                                 );
      pvci->psvgUserData     = XML_GetUserData( pvci->p );
      pvci->status           = VC_VALIDATING;
      if (pvci->pXMLUserData == NULL)
        {
          pvci->pXMLUserData = pvci->psvgUserData;
        }
      XML_SetUserData(pvci->p, pvci);


      if (pvci->pStack->pThis->pCallbacks        !=NULL &&
          pvci->pStack->pThis->pCallbacks->start !=NULL )
        {
          pvci->pStack->pThis->pCallbacks->start( pvci->pXMLUserData, 
                                                  pvci->pStack->pThis->p.name,
                                                  NULL
                                                );
        }
    }
}

/*====================================================================================\
| Page: 12 |                                                                          |
|==========/                                                                          |
| Function: VC_ValidatorFree()                                                        |
|==========                                                                           |
|                                                                                     |
| Description:                                                                        |
|=============                                                                        |
| This function frees memory used by the validator when validation is over.           |
|                                                                                     |
| It MUST NOT be called if VC_ValidationCreate failed (returned NULL).                |
| It MUST be called BEFORE releasing the parser with XML_ParserFree, because it relies|
| on the parser for its internal structures.                                          |
| It MUST be called wether the validation succeeded or not, for proper memory         |
| reclamation.                                                                        |
| The function will restore the UserData to the original value (the one before the    |
| call to VC_ValidatorStart), if not already changed. It cannot do so with handlers   |
| callbacks because expat provides no way (except hacking into its internal code!) to |
| read the value of these function pointer. If we were validating the whole document  |
| there should be no issue because parsing will be ended too, so we just call this    |
| function then we call XML_ParserFree.                                               |
| But if we did a validation of a sub-tree, to continue useful parsing, the "normal"  |
| method is to set a custom endElement handler in the topElement (passed  to          |
| VC_ValidatorCreate). Then, in this handler, you set the new handlers for the rest   |
| of your parsing and call this function. It will detect this situation and postpone  |
| the free-ing process to a position where it can be done safely.                     |
| Doing otherwise is unsafe because the parser may continue to call the callbacks set |
| by our validator although you have changed them.                                    |
\====================================================================================*/
extern void XMLCALL
VC_ValidatorFree( VC_Validator pvci )
{
  VC_Stack *pOld;

  if (pvci != NULL)
    {
      if ( (pvci->flags & _vc_F_LAST_CALLBACK) == _vc_F_LAST_CALLBACK &&
           pvci->status != VC_FINISHED
         )
        {
          pvci->flags |= _vc_F_FREE_REQUESTED;
          return;
        }

      if ( XML_GetUserData(pvci->p) == pvci )
        {
          XML_SetUserData(pvci->p, pvci->psvgUserData);
        }

      for( pOld= pvci->pStack; pOld != NULL; pOld= pvci->pStack )
        {
          pvci->pStack= pvci->pStack->pParent;
          XML_MemFree(pvci->p, pOld);
        }

      XML_MemFree(pvci->p, pvci);
    }
}

/*====================================================================================\
| Page: 13 |                                                                          |
|==========/                                                                          |
| Function: VC_SetUserData(), VC_GetUserData(), VC_UseValidatorAsHandlerArg()         |
|==========                                                                           |
|                                                                                     |
| Description:                                                                        |
|=============                                                                        |
| These functions are exact counterparts of the XML_ expat functions.                 |
|                                                                                     |
| When using the validator, the XML_ MUST NOT be used to Get/Set the userdata,        |
| instead these ones must be used. They have the same parameters and functionnality   |
| as their expat counterparts, and they use a VC_Validator as first parameter         |
| instead of a XML_Parser.                                                            |
\====================================================================================*/

extern void XMLCALL
VC_SetUserData( VC_Validator pvci,
                void        *userData )
{
  if ( pvci!= NULL)
    {
      pvci->pXMLUserData= userData;
    }
}

extern void * XMLCALL
VC_GetUserData( VC_Validator pvci )
{
  if ( pvci== NULL)
    return NULL;
  else
    return pvci->pXMLUserData;
}


extern void XMLCALL
VC_UseValidatorAsHandlerArg( VC_Validator pvci )
{
  if ( pvci!= NULL)
    {
      pvci->pXMLUserData= pvci;
    }
}


/*====================================================================================\
| Page: 14 |                                                                          |
|==========/                                                                          |
| Function: VC_GetErrorCode(),  VC_ErrorString(), VC_ExpatValidatorVersion()          |
|==========                                                                           |
|                                                                                     |
| Description:                                                                        |
|=============                                                                        |
| These functions are exact counterparts of the XML_ expat functions.                 |
|                                                                                     |
| According to what you want to achieve, you can use both these functions and the     |
| XML_ ones. The VC_ functions below gives error codes and messages about validation  |
| whereas the XML_ expat functions report well-formedness errors.                     |
\====================================================================================*/

extern enum VC_Error XMLCALL
VC_GetErrorCode( VC_Validator pvci )
{
  if ( pvci== NULL)
    return VC_ERROR_INTERNAL_ERROR;
  else
    return pvci->errorCode;
}

extern const XML_LChar * XMLCALL
VC_ErrorString( enum VC_Error errCode )
{
  if (errCode < VC_ERROR_NONE || errCode >VC_ERROR_INTERNAL_ERROR)
    return pVC_ErrorStrings[VC_ERROR_INTERNAL_ERROR - VC_ERROR_NONE + 1];

  return pVC_ErrorStrings[errCode - VC_ERROR_NONE];
}

extern XML_LChar * XMLCALL
VC_ExpatValidatorVersion( void )
{
  return "expat-validator_1.0.2";
}



/*====================================================================================\
| Page: 15 |                                                                          |
|==========/                                                                          |
| Function: VC_StopValidator(), VC_GetValidationStatus()                              |
|==========                                                                           |
|                                                                                     |
| Description:                                                                        |
|=============                                                                        |
| These functions are counterparts of the XML_ expat functions.                       |
|                                                                                     |
| VC_StopValidator, stops validating. It returns an error if validation is already    |
|  finished or if passed a NULL pointer as validator. If OK, the error code is set    |
|  to VC_ERROR_ABORTED.                                                               |
|  IMPORTANT: note that it does not change the handlers nor the userdata, thus if     |
|  the caller does not change anything else the parsing will transparently continue   |
|  up to the end of file (or stop on well-formed error). Thus the caller will         |
|  probably want to stop parsing after this call, or to set handlers/userdata to      |
|  continue parsing (see example in the test case program).                           |
|  //TODO the resumable flag and the status VC_SUSPENDED are not coded yet.           |
|                                                                                     |
| VC_GetValidationStatus, returns the validation status, and a flag if we reached the |
|  last element.                                                                      |
\====================================================================================*/

extern enum XML_Status XMLCALL
VC_StopValidator( VC_Validator pvci, 
                  VC_Bool      future_use_resumable )
{
  (void) future_use_resumable; /* Don't warn, this argument is not used now but      */
                               /* could be used in the future, see TODO              */

  if ( pvci!= NULL)
    {
      if (pvci->status == VC_FINISHED)
        {
          pvci->errorCode= VC_ERROR_ALREADY_FINISHED;
        }
      else
        {
          pvci->status = VC_FINISHED;
          pvci->errorCode= VC_ERROR_ABORTED;
          return XML_STATUS_OK;
        }
    }  
  return XML_STATUS_ERROR;
}

extern void XMLCALL
VC_GetValidationStatus( VC_Validator         pvci,
                        VC_ValidationStatus *pStatus )
{
  if ( pvci != NULL && pStatus != NULL )
    {
      pStatus->validating   = pvci->status;
      pStatus->finalCallback= (pvci->flags) ? VC_TRUE : VC_FALSE;
    }
}


/*====================================================================================\
| Page: 16 |                                                                          |
|==========/                                                                          |
| Function: VC_GetCurrentStack()                                                      |
|==========                                                                           |
|                                                                                     |
| Description:                                                                        |
|=============                                                                        |
| These functions are additions for the validation process.                           |
|                                                                                     |
| VC_GetCurrentStack returns the pointer to the current stack of elements. It can     |
| be helpful to know how deep we are and where exactly, inside the XML hierarchy.     |
\====================================================================================*/

extern VC_Stack * XMLCALL
VC_GetCurrentStack( VC_Validator pvci )
{
  return ( pvci== NULL ) ? NULL : pvci->pStack;
}
